#MediaStack - API Automation
https://mediastack.com/documentation


##Execution
 - To execute all of test cases, run command : `mvn clean verify`
 - To execute specific `Task 1` test case, run command : `mvn clean verify -Dtags="@Task1"`
 - To execute specific `Task 2` test case, run command : `mvn clean verify -Dtags="@Task2"`


##Reporting
The report automatically generated when test execution finished, check on : `target/site/serenity/index.html`


##What wee tests
- `Task 1` Filter articles by keyword and country and check json schema, http://api.mediastack.com/v1/news?access_key=YOUR_ACCESS_KEY&keywords=KEYWORD&countries=COUNTRY
- `Task 2` Check response code with valid/invalid api parameters, http://api.mediastack.com/v1/news?access_key=YOUR_ACCESS_KEY&countries=COUNTRY

##How to add new tests
1. Add api endpoint to file `main/java/com.mediastack.app.pages/MediaStackController.java`
2. Create ".feature" file with scanario in `test/resources/features.mediastack`
3. Create steps definition matched feature in `test/java/com.mediastack/steps`

