Feature: API Automation for MediaStack

  @Task1
  Scenario: Check response code and json schema for API
    Given   User check API authentication
     Then   Check response status code
      And   Check articles json scheme

  Scenario: Filter articles by country and keyword
    Given   User filter by keyword  and country
      | keywords  | tennis |
      | countries | us     |
     When   User extract filtered articles
     Then   Check filtered articles contains same country and keyword


  @Task2
  Scenario Outline: Check response code with valid/invalid api parameters
    Given   User check API authentication with valid or invalid "<key>" "<value>"
     When   User get response
     Then   User check response <code>
    Examples:
      | key        | value | code |
      | access_key | us    | 401  |
      | countries  | us1   | 422  |
      | countries  | us    | 200  |