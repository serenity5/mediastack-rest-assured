package com.mediastack.steps;

import com.mediastack.app.model.ApiModel;
import com.mediastack.app.model.Article;
import com.mediastack.app.pages.MediaStackController;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import net.thucydides.core.annotations.Steps;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class MediaStackDefinitions {
    @Steps
    private final MediaStackController mediaStackController = new MediaStackController();
    ValidatableResponse validatableResponse;
    ExtractableResponse<Response> response;
    List<Article> articles;
    List<Article> filteredArticles;
    String keywords;
    String countries;

    @Given("User check API authentication")
    public void userGetAllArticles() {
        validatableResponse = mediaStackController.getAllArticles();
    }

    @Then("Check response status code")
    public void filteredArticleSchemeShouldBe() {
        assertEquals(200, validatableResponse.extract().statusCode());
    }

    @And("Check articles json scheme")
    public void filteredArticlesSchemeShouldBe() {
        validatableResponse.assertThat().body(JsonSchemaValidator.matchesJsonSchemaInClasspath("scheme/apiResponseScheme.json"));
    }

    @Given("User filter by keyword  and country")
    public void userGetFilteredArticles(DataTable args) {
        Map<String, String> params = args.asMap(String.class, String.class);
        validatableResponse = mediaStackController.getArticlesByQuery(params);
        this.keywords = params.get("keywords");
        this.countries = params.get("countries");
    }

    @When("User extract filtered articles")
    public void userGetFilteredArticles() {
        articles  = validatableResponse.extract().as(ApiModel.class).getArticles().stream()
                .sorted(Comparator.comparing(Article::getTitle)).collect(Collectors.toList());
        filteredArticles  = articles.stream()
                .filter(article -> checkArticle(article, keywords, countries))
                .sorted(Comparator.comparing(Article::getTitle)).collect(Collectors.toList());
    }

    @Then("Check filtered articles contains same country and keyword")
    public void filteredArticleShouldBe() {
        assertEquals(articles, filteredArticles);
    }

    @Given("User check API authentication with valid or invalid {string} {string}")
    public void userMakeRequestWithParameters(String key, String value) {
        validatableResponse = mediaStackController.getArticlesByQueryKeyword(key, value);
    }

    @When("User get response")
    public void userMakeRequestWithParameters() {
         response = validatableResponse.extract();
    }

    @Then("User check response {int}")
    public void checkResponseStatusCode(int code) {
        assertEquals(code, response.statusCode());
    }

    boolean checkArticle(Article article, String keyword, String country){
        return  article.getCountry().equals(country) && (article.getDescription().toLowerCase().contains(keyword.toLowerCase()) ||
                article.getTitle().toLowerCase().contains(keyword.toLowerCase()));
    }
}
