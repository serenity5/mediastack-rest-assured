package com.mediastack.app.pages;

import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.given;

public class MediaStackController {
    private static final String ACCESS_KEY = "access_key";
    private static final String NEWS = "/news";
    private final String baseUri = "http://api.mediastack.com/v1/";
    private final String accessKey = "62457e3c0ef76dde6b630486d537b3de";
    RequestSpecification requestSpec = given()
            .baseUri(baseUri)
            .log().all()
            .queryParam(ACCESS_KEY, accessKey)
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON);

    public ValidatableResponse getAllArticles() {
        return requestSpec
                .when()
                .get(NEWS)
                .then()
                .log().all();
    }

    public ValidatableResponse getArticlesByQueryKeyword(String key, String value) {
        HashMap<String, String> params = new HashMap<>();
        params.put(key, value);
        return getArticlesByQuery(params);
    }

    public ValidatableResponse getArticlesByQuery(Map<String, String> params) {
        params.forEach((key, value) -> requestSpec.queryParam(key, value));
        return requestSpec
                .get(NEWS)
                .then()
                .log().all();
    }
}
